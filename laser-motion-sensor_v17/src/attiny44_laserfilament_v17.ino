/*
 * FilamentSensor.cpp
 *
 * Created: 20/04/2017 21:41:39
 * Authors: tony@think3dprint3d.com and dcrocker@eschertech.com
 
 Library and example code from jkl http://www.cs.cmu.edu/~dst/ARTSI/Create/PC%20Comm/
 and from the arduino version more focused on the ATTiny85 http://playground.arduino.cc/Code/USIi2c
 
 2017-08 12 Changed bit rate to 2000bps

 */ 

#include "ecv.h"

#ifdef __ECV__
#define __attribute__(_x)
#define __volatile__
#pragma ECV noverifyincludefiles
#endif

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>
#include <avr/fuse.h>
#include <Arduino.h>

#ifdef __ECV__
#pragma ECV verifyincludefiles
#undef cli
#undef sei
extern void cli();
extern void sei();
#endif

//for setting register bits with AVR code
//cbi and sbi are standard (AVR) methods for setting, or clearing, bits in PORT (and other) variables. 
#ifndef cbi
  #define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#endif

#ifndef sbi
  #define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))
#endif 

#include "PAT9125.h"
#include "USI_TWI_Master.h"                  // for _delay_us

#define DOUBLE_SPEED  (0)                 // set nonzero for 2000bps, zero for 1000bps

#define BITVAL(_x) static_cast<uint8_t>(1u << (_x))

//const uint32_t F_CPU = 8000000UL;
#ifdef F_CPU
  #undef F_CPU
  #define F_CPU 8000000UL
#endif


// define pins and constants
const unsigned int PortOutBitNum = 7;
const unsigned int PortSwitchBitNum = 2;          // optional filament present switch, has an external pullup resistor

const unsigned int PortLedRedBitNum = 1;          // red LED
const unsigned int PortLedGreenBitNum = 0;          // green LED
const unsigned int PortMotionInputBitNum = 2;

const unsigned int Pad0BitNum = 0;              // pad 0 on underside
const unsigned int Pad1BitNum = 1;              // pad 1 on underside
const unsigned int PIN_LED_BLUE = 1;
const unsigned int PIN_LED_GREEN = 0;

const unsigned int PIN_STP = 10;
const unsigned int PIN_DIR = 9;
const unsigned int PIN_OUT = 3; // output pin for error signal

const int SENSOR_TICKS_PER_MM = 50;
const int CNC_STEPS_PER_MM = 186; // take *2 because ISR triggers on both rising and falling edge of step signal
const float MOTION_THRESHOLD = 0.1; // threshold for motion error. ratio between cnc steps and motion
// map pins to ports
const uint8_t LedRed = BITVAL(PortLedRedBitNum);
const uint8_t LedGreen = BITVAL(PortLedGreenBitNum);

const uint8_t PortADdrBits = BITVAL(PortOutBitNum);
const uint8_t PortBDdrBits = LedRed | LedGreen;

const uint8_t PortAPullupBits = BITVAL(Pad0BitNum) | BITVAL(Pad1BitNum) | BITVAL(PortSwitchBitNum) | BITVAL(3);   // unused pins, enable pullups on them
const uint8_t PortBPullupBits = BITVAL(PortMotionInputBitNum);    // input pins that need pullups enabled

#define PORT_LED  PORTB
#define PORT_OUT  PORTA
#define PORT_PADS PORTA
#define PIN_SWITCH  PINA
#define PIN_PADS  PINA
#define DDR_PADS  DDRA

#if DOUBLE_SPEED
  const uint16_t TicksPerSecond = 2000;           // this must be the same as the desired bit rate
#else
  const uint16_t TicksPerSecond = 1000;           // this must be the same as the desired bit rate
#endif

// Error codes (expressed as number of blinks
const uint8_t FLASHES_OK = 3;
const uint8_t FLASHES_ERR_INIT = 5;

#if DOUBLE_SPEED
  const uint16_t MinOutputIntervalTicks = TicksPerSecond/50;  // send the angle 50 times per second while it is changing
  const uint16_t MinPulseIntervalTicks = TicksPerSecond/400;  // max 200 output transitions/sec in pulse mode (100mm/sec)
#else
  const uint16_t MinOutputIntervalTicks = TicksPerSecond/10;  // send the angle 10 times per second while it is changing
  const uint16_t MinPulseIntervalTicks = TicksPerSecond/200;  // max 200 output transitions/sec in pulse mode (100mm/sec)
#endif

const uint16_t MaxOutputIntervalTicks = TicksPerSecond/2; // send the angle at least every half second

const uint16_t kickFrequency = 10;              // how often we kick the watchdog
const uint16_t kickIntervalTicks = TicksPerSecond/kickFrequency;

const uint16_t ParityBit = 0x8000u;             // adjusted so that there is an even number of bits
const uint16_t QualityBit = 0x4000u;            // set in words containing status information, clear in words containing position information
const uint16_t ErrorBit = 0x2000u;              // set if the sensor failed to initialise or self-test
const uint16_t SwitchOpenBit = 0x1000u;           // set if the optional switch is open, or the switch not connected

const uint16_t ErrorBlinkTicks = TicksPerSecond/4;      // fast error blinks
const uint16_t OkBlinkTicks = TicksPerSecond/4;       // fast OK blinks

#ifndef __ECV__
//FUSES = {0xE2u, 0xDFu, 0xFFu};    // 8MHz RC clock
#endif

// define variables
uint16_t lastOutval = 0;
volatile uint16_t tickCounter = 0;
uint16_t lastKickTicks = 0;
uint16_t lastPollTicks = 0;
uint16_t lastOutTicks = 0;
bool usePulseMode = false;

volatile long cncSteps = 0;
long sensor_ticks = 0;
float cnc_motion = 0;
float sensor_motion = 0;

// Forward declarations
void blink(uint8_t count, uint8_t leds, uint16_t delayOn, uint16_t delayOff);

// Get a 16-bit volatile value from outside the ISR. As it's more than 8 bits long, we need to disable interrupts while fetching it.
inline uint16_t GetVolatileWord(volatile uint16_t& val)
writes(volatile)
{
  cli();
  const uint16_t locVal = val;
  sei();
  return locVal;
}

// Check whether we need to kick the watchdog
void CheckWatchdog()
writes(lastKickTicks; volatile)
{
  if (GetVolatileWord(tickCounter) - lastKickTicks >= kickIntervalTicks)
  {
#ifndef __ECV__
    wdt_reset();                      // kick the watchdog
#endif
    lastKickTicks += kickIntervalTicks;
  }
}

// Delay for a specified number of ticks, kicking the watchdog as needed
void DelayTicks(uint16_t ticks)
writes(lastKickTicks; volatile)
{
  const uint16_t startTicks = GetVolatileWord(tickCounter);
  for (;;)
  {
    CheckWatchdog();
    if (GetVolatileWord(tickCounter) - startTicks >= ticks)
    {
      break;
    }
  }
}

// Report an error. The output must have been in the low state for at least 10 ticks before calling this.
void ReportError(uint8_t errorNum)
{
  blink(errorNum, LedRed, ErrorBlinkTicks, ErrorBlinkTicks);  // short error blinks, also leaves the output in the low state for ErrorBlinkTicks
}

int main(void)
{  
  // Set up the I/O ports
  PORTA = PortAPullupBits;
  PORTB = PortBPullupBits;
  DDRA = PortADdrBits;
  DDRB = PortBDdrBits;

  // Setup the timer to generate the tick interrupt
#if DOUBLE_SPEED
  // For a tick rate of 2000Hz we need a total divisor of 4000, for example 250 * 16 or 125 * 32.
  // Timer/counter 0 only offers prescalers of 1, 8, 64, 256 and 1024. But we can get 16 by using dual slope mode and prescaler 8.
  TCCR0A = BITVAL(WGM00);                   // phase correct PWM mode, count up to OCR0A then count down
  TCCR0B = BITVAL(WGM02) | BITVAL(CS01);            // prescaler 8
  OCR0A = F_CPU/(16 * TicksPerSecond) - 1;          // set the period to the bit time
#else
  // For a tick rate of 1000 we need a total divisor of 8000, for example 125 * 64
  TCCR0A = BITVAL(WGM01);                   // CTC mode, count up to OCR0A then start again from zero
  TCCR0B = BITVAL(CS01) | BITVAL(CS00);           // prescaler 64
  OCR0A = F_CPU/(64 * TicksPerSecond) - 1;          // set the period to the bit time
#endif
  TCNT0 = 0;
  TIMSK0 |= BITVAL(OCIE0A);                 // enable timer compare match interrupt

#ifndef __ECV__                         // eCv++ doesn't understand gcc assembler syntax
  wdt_enable(WDTO_500MS);                   // enable the watchdog
#endif

pinMode(PIN_STP,INPUT);
pinMode(PIN_DIR, INPUT);
pinMode(PIN_LED_BLUE,OUTPUT);
pinMode(PIN_LED_GREEN,OUTPUT);
//attachInterrupt(PIN_STP,stepISR,CHANGE);

// enable pin change interrupt on PCINT7:0
sbi(GIMSK,PCIE0);
// enable pin change interrupt on pin PCINT0
sbi(PCMSK0,PCINT0);

  sei();

  for (;;)
  {
    PORT_OUT &= ~BITVAL(PortOutBitNum);           // ensure output is in default low state
    DelayTicks(2 * TicksPerSecond);             // allow the power voltage to stabilise, or give a break from flashing the previous error (also kicks the watchdog)
    
    
    if (OTS_Sensor_Init())
    {
      break;
    }
    ReportError(FLASHES_ERR_INIT);
  }

  blink(FLASHES_OK, LedGreen, OkBlinkTicks, OkBlinkTicks);  // blink 3 times after successful initialisation

  PORT_OUT &= ~BITVAL(PortOutBitNum);             // ensure output is in default low state 

  int16_t currentSteps = 0;
  bool sentPosition = false;
  for (;;)
  {
    CheckWatchdog();

    const uint16_t now = GetVolatileWord(tickCounter);
    const uint16_t diff = now - lastPollTicks;        // how long since we polled the sensor


    if (diff >= MinPulseIntervalTicks)
    {
      // Update the current filament position
      int16_t dx, dy;
      if (OTS_Sensor_ReadMotion(dx, dy))
      {
        currentSteps += dy;       // we use the Y motion and we are assuming 2's complement representation here
      }
      lastPollTicks = now;
      /*
      if (currentSteps<0) {
        digitalWrite(1,HIGH);
        digitalWrite(0,LOW);
        // blink(FLASHES_OK, LedGreen, OkBlinkTicks, OkBlinkTicks);
      } 
      if (currentSteps>0) {
        digitalWrite(1,LOW);
        digitalWrite(0,HIGH);
      }
      if (currentSteps == 0) {
        digitalWrite(0,LOW);
        digitalWrite(1,LOW);
      }
      */
      sensor_ticks = sensor_ticks + currentSteps;
      sensor_motion = (float)sensor_ticks / (float)SENSOR_TICKS_PER_MM;
      currentSteps = 0;
      
      // check error every mm
      if ( cncSteps > CNC_STEPS_PER_MM ) {
      //update blinking LEDs here

        // calculate cnc steps in mm
        cnc_motion = (float)cncSteps / (float)CNC_STEPS_PER_MM;
        if ( ( (cnc_motion - sensor_motion) / cnc_motion) > MOTION_THRESHOLD )
        {
          digitalWrite(PIN_OUT,HIGH);
          blink(1, LedRed, 10, 10);
        }
        
      }
      
      /*
      // test LEDs
      if(digitalRead(PIN_DIR) ) {
        digitalWrite(PIN_LED_BLUE,HIGH);
      }
      if(digitalRead(PIN_STP)) {
        digitalWrite(PIN_LED_GREEN,HIGH);
      }
      delay(20);
      digitalWrite(PIN_LED_BLUE,LOW);
      digitalWrite(PIN_LED_GREEN,LOW);
      */

      // reset LEDs set in interrupt
      digitalWrite(PIN_LED_BLUE,LOW);
      digitalWrite(PIN_LED_GREEN,LOW);
    }
  }

#ifdef __ECV__
  return 0;
#endif
}

// Timer ISR for setting output flag
#ifdef __ECV__
void TIM0_COMPA_vect()
#else
ISR(TIM0_COMPA_vect)
#endif
{
  tickCounter++;
}

// Wait for the next tick. This does not call the watchdog, so don't call this too many times without making a call to checkWatchdog.
inline void WaitForNextTick()
{
  const volatile uint8_t * const tickLsb = reinterpret_cast<const volatile uint8_t *>(&tickCounter);
  const uint8_t initialCount = *tickLsb;
  while (*tickLsb == initialCount) { }
}


/*------------------------------------------------------------------------
**  blinkCustom - function to blink LED for count passed in
**    Assumes that leds are all on the same port. 
**     Custom on and off times can be set in ~0.1s increments
** ---------------------------------------------------------------------*/
void blink(uint8_t count, uint8_t ledMask, uint16_t delayOn, uint16_t delayOff)
{
  while (count != 0)
  {
    PORT_LED |= ledMask;
    DelayTicks(delayOn);
    PORT_LED &= ~ledMask;
    DelayTicks(delayOff);
    count--;
  }
}

//void stepISR() {
ISR(PCINT0_vect) {
  if(digitalRead(PIN_DIR) ) {
    // forward motion
    cncSteps = cncSteps + 1;
    blink(1, LedRed, 5, 5);
  }
  else {
    // backwards motion
    cncSteps = cncSteps - 1;
    blink(1, LedGreen, 5, 5);
  }
}
// End
