# laser-motion-sensor

A smart motion sensor using a laser motion sensor from pixart.

This project is built upon the [duet3d smart filament sensor](https://duet3d.dozuki.com/Wiki/Duet3dFilamentMonitor_LaserVersion) and the [smart filament sensor from Ludwig3D on thingiverse](https://www.thingiverse.com/thing:3926841)


# Setup and Installation
## general information
add this url in the Arduino IDE settings to enable ATtiny support:
http://drazzy.com/package_drazzy.com_index.json

to set up different ISPs with Platformio see https://docs.platformio.org/en/latest/platforms/atmelavr.html

## Windows
### using USBasp
1. Download [zadic](http://zadig.akeo.ie/), install and
1. choose libusbK (v3.0.7.0) as Driver and hit install (other instructions say libusb-win32, but [this post](https://quadmeup.com/working-solution-for-usbasp-driver-in-windows-10-64bit/)  says libusbk is the right one)
1. flash using Arduino IDE

### using Arduino ISP
was not working with an Arduino Leonardo and Platformio using VS Code. Only working from Arduino IDE directly.

## mac

## wiring
on the SKR1.4 board the step and dir pin are located on the pin headers next to the motor drivers, as can be seen in this [pinout diagram](https://github.com/bigtreetech/BIGTREETECH-SKR-V1.3/blob/master/BTT%20SKR%20V1.4/Hardware/SKR-V1.4-Turbo-pinout.jpg).
