#include <Arduino.h>
#include "Pins.h"
#include "PAT9130.h"
/*
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>
#include <avr/fuse.h> 
*/

// pin definitions
const int PIN_LED_GREEN = 1; // platformio uses CCW pin numbering!
const int PIN_LED_RED = 2;

// constants
const int SENSOR_READ_INTERVAL_MS = 50;

// variables
int16_t dx = 0;
int16_t dy = 0;

int current_steps_x = 0;  
int current_steps_y = 0;  

unsigned long timestamp = 0;

void setup() {
  // Set up the I/O ports (imported from duet3D sketch)
	// PORTA = BITVAL(PortASwitchBitNum) | BITVAL(PortANCSBitNum) | BITVAL(PortASCLBitNum);	// ensure NCS doesn't go low when we set it to be an output; laser is off
	// PORTB = BITVAL(PortBMFIOInputBitNum);						// set pullup on MFIO
	DDRA = BITVAL(PortAOutBitNum) | BITVAL(PortANCSBitNum) | BITVAL(PortASCLBitNum) | BITVAL(PortALDPBitNum);

  pinMode(PIN_LED_GREEN,OUTPUT);
  pinMode(PIN_LED_RED,OUTPUT);
  if(PAT9130Init()){
    for(int i=0; i<3; i++){
      digitalWrite(PIN_LED_GREEN,HIGH);
      delay(100);
      digitalWrite(PIN_LED_GREEN,LOW);
      delay(100);
    }
  } else {
    for(int i=0; i<3; i++){
      digitalWrite(PIN_LED_RED,HIGH);
      delay(100);
      digitalWrite(PIN_LED_RED,LOW);
      delay(100);
    }
  }
}

void loop() {
  
  current_steps_x = 0;
  dx=0;
  current_steps_y = 0;
  dy=0;

  if( (millis() - timestamp) > SENSOR_READ_INTERVAL_MS ){
    timestamp = millis();
   
    if (PAT9130ReadMotion(dx, dy)) {
	  	current_steps_y = (int)dy;
 	  	current_steps_x = (int)dx;
	  }

    if( current_steps_x > 20 || current_steps_x < -20){
     digitalWrite(PIN_LED_RED,HIGH);  
    }

    if( current_steps_y > 20 || current_steps_y < -20){
     digitalWrite(PIN_LED_GREEN,HIGH);  
    }
    delay(10);
    digitalWrite(PIN_LED_GREEN,LOW);
    digitalWrite(PIN_LED_RED,LOW);
  }

}